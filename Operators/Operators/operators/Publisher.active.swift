//
//  Publisher.active.swift
//
//  Created by Roland Schmitz on 29.03.20.
//
//  MIT License
//
//  Copyright (c) 2020 Roland Schmitz
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//      furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.

import Foundation
import Combine

extension Publisher {
    /// Creates a Publisher which publishes true if the upstream publisher is active and false after a timeout.
    /// The publisher never sends the same active state twice in a row.
    /// - Parameters:
    ///   - timeout: The time the publisher should wait for new input from the upstream publish before considering
    ///     it as not active and publishing false.
    ///   - scheduler: The scheduler on which this publisher delivers elements
    ///   - options: Scheduler options that customize this publisher’s delivery of elements.
    /// - Returns: A publisher which publishes true if upstream publisher is publishing anything and publishes false after a timeout
    public func active<S, F>(timeout: S.SchedulerTimeType.Stride, scheduler: S, options: S.SchedulerOptions? = nil) -> AnyPublisher<Bool, F>
        where F == Self.Failure, S : Scheduler {
        let sharedInputPublisher = self.share()
        return sharedInputPublisher
            .map { _ in true }
            .merge(with: sharedInputPublisher
                .map { _ in false }
                .debounce(for: timeout, scheduler: scheduler, options: options) )
            .removeDuplicates()
            .eraseToAnyPublisher()
    }
}

