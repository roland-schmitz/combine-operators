//
//  Publisher.countDuring.swift
//
//  Created by Roland Schmitz on 29.03.20.
//
//  MIT License
//
//  Copyright (c) 2020 Roland Schmitz
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//      furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.

import Foundation

import Combine

extension Publisher where Failure == Never {

    /// Creates a publisher which counts how often the upstream publisher publishes during a given interval
    /// - Parameters:
    ///   - interval: The duration in which the publisher should count before publishing the next result.
    ///   - queue: The OperationQueue on which this publisher delivers elements
    ///   - options: Scheduler options that customize this publisher’s delivery of elements.
    /// - Returns: A publisher which counts how often the upstream publisher publishes during a given interval
    public func countDuring(interval: TimeInterval, queue: DispatchQueue) -> AnyPublisher<Int, Never> {
            self
                .map { _ in 1 }
                .merge(with: Timer
                    .publish(every: interval, on: RunLoop.main, in: .default).autoconnect()
                    .map { _ in 0 } )
                .collect(Publishers.TimeGroupingStrategy.byTime(queue, .init(floatLiteral: interval)))
                .map { numbers in numbers.reduce(0,+) }
                .eraseToAnyPublisher()
    }
}

