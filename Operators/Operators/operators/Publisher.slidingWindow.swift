//
//  Publisher.slidingWindow.swift
//
//  Created by Roland Schmitz on 30.03.20.
//
//  MIT License
//
//  Copyright (c) 2020 Roland Schmitz
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//      furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.

import Foundation
import Combine

extension Publisher where Failure == Never {
    
    /// Creates a publisher which collects the items of an upstream publisher in a sliding window
    /// - Parameters:
    ///   - count: maximum number of elements of the resulting array
    /// - Returns: A publisher which collects the items of an upstream publisher in a sliding window
    public func slidingWindow(count: Int) -> AnyPublisher<[Output], Never> {
        // todo: use linked list for performance
        // todo: what should happen when count is negative
        // todo: fix bad behaviour for count = 0
        // todo: support any Failure type
        self
            .scan([]) { previousResult, input in
                previousResult.dropFirst(count > previousResult.count ? 0 : 1) + [ input ]
        }
        .eraseToAnyPublisher()
    }
}

