//
//  Publisher.slidingAverage.swift
//
//  Created by Roland Schmitz on 23.03.20.
//
//  MIT License
//
//  Copyright (c) 2020 Roland Schmitz
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//      furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.

import Foundation
import Combine

extension Publisher where Output == Double, Failure == Never {
    /// Creates a publisher which calculates an average on a sliding window of the last values received from the upstream publisher
    /// if there are not yet values to fill the window the average will be calculated with all received values
    ///
    /// - Parameter count: window size
    /// - Returns: Publisher which publishes the average of last count values received
    public func slidingAverage(count: Int) -> AnyPublisher<Double, Never> {
        self
            .slidingWindow(count: count)
            .map { windowValues in windowValues.reduce(0.0,+) / Double(windowValues.count) }
            .eraseToAnyPublisher()
    }
}

