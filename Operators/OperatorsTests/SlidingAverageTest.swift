//
//  SlidingAverageTest.swift
//
//  Created by Roland Schmitz on 04.04.20.
//
//  MIT License
//
//  Copyright (c) 2020 Roland Schmitz
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//      furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.
//

import XCTest
import Combine

class SlidingAverageTest: XCTestCase {

    let upstream = PassthroughSubject<Double, Never>()
    var receivedValues: [Double] = []
    var cancellable: AnyCancellable?

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func setupSlidingWindow(count: Int) {
        cancellable = upstream
            .slidingAverage(count: count)
            .print(self.debugDescription)
            .sink { receivedValue in
                self.receivedValues.append(receivedValue)
            }
    }

    func testNegativeWindow() throws {
        // todo: can we avoid negative window size
    }

    func testWindow0Receive() throws {
        // todo: how can we deal with count 0
    }

    func testWindow1Receive() throws {
        setupSlidingWindow(count: 1)
        upstream.send(1.0)
        XCTAssertEqual(receivedValues, [1.0])
        upstream.send(5.0)
        XCTAssertEqual(receivedValues, [1.0, 5.0])
        upstream.send(3.0)
        XCTAssertEqual(receivedValues, [1.0, 5.0, 3.0])
    }

    func testWindow2Receive() throws {
        setupSlidingWindow(count: 2)
        upstream.send(1.0)
        XCTAssertEqual(receivedValues, [1.0])
        upstream.send(5.0)
        XCTAssertEqual(receivedValues, [1.0, 3.0])
        upstream.send(3.0)
        XCTAssertEqual(receivedValues, [1.0, 3.0, 4.0])
        upstream.send(7.0)
        XCTAssertEqual(receivedValues, [1.0, 3.0, 4.0, 5.0])
    }

    func testWindow3Receive() throws {
        setupSlidingWindow(count: 3)
        upstream.send(1.0)
        XCTAssertEqual(receivedValues, [1.0])
        upstream.send(5.0)
        XCTAssertEqual(receivedValues, [1.0, 3.0])
        upstream.send(6.0)
        XCTAssertEqual(receivedValues, [1.0, 3.0, 4.0])
        upstream.send(7.0)
        XCTAssertEqual(receivedValues, [1.0, 3.0, 4.0, 6.0])
        upstream.send(2.0)
        XCTAssertEqual(receivedValues, [1.0, 3.0, 4.0, 6.0, 5.0])
    }

}
