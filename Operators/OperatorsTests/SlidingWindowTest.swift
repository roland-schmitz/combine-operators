//
//  SlidingWindowTest.swift
//
//  Created by Roland Schmitz on 04.04.20.
//
//  MIT License
//
//  Copyright (c) 2020 Roland Schmitz
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//      furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.
//

import XCTest
import Combine

class SlidingWindowTest: XCTestCase {

    let upstream = PassthroughSubject<Int, Never>()
    var receivedValues: [[Int]] = []
    var cancellable: AnyCancellable?

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func setupSlidingWindow(count: Int) {
        cancellable = upstream
            .slidingWindow(count: count)
            .print(self.debugDescription)
            .sink { receivedValue in
                self.receivedValues.append(receivedValue)
            }
    }

    func testNegativeWindow() throws {
        // todo: fix slidingWindow implementation: how can we avoid or deal with a negative count
        XCTAssertThrowsError( setupSlidingWindow(count: -1) )
    }

    func testWindow0Receive() throws {
        setupSlidingWindow(count: 0)
        upstream.send(1)
        // todo: fix slidingWindow implementation for count 0
        XCTAssertEqual(receivedValues, [[]])
        upstream.send(2)
        XCTAssertEqual(receivedValues, [[],[]])
        upstream.send(2)
        XCTAssertEqual(receivedValues, [[],[],[]])
    }

    func testWindow1Receive() throws {
        setupSlidingWindow(count: 1)
        upstream.send(1)
        XCTAssertEqual(receivedValues, [[1]])
        upstream.send(2)
        XCTAssertEqual(receivedValues, [[1],[2]])
        upstream.send(3)
        XCTAssertEqual(receivedValues, [[1],[2],[3]])
    }

    func testWindow2Receive() throws {
        setupSlidingWindow(count: 2)
        upstream.send(1)
        XCTAssertEqual(receivedValues, [[1]])
        upstream.send(2)
        XCTAssertEqual(receivedValues, [[1],[1,2]])
        upstream.send(3)
        XCTAssertEqual(receivedValues, [[1],[1,2],[2,3]])
        upstream.send(4)
        XCTAssertEqual(receivedValues, [[1],[1,2],[2,3],[3,4]])
    }

    func testWindow3Receive() throws {
        setupSlidingWindow(count: 3)
        upstream.send(1)
        XCTAssertEqual(receivedValues, [[1]])
        upstream.send(2)
        XCTAssertEqual(receivedValues, [[1],[1,2]])
        upstream.send(3)
        XCTAssertEqual(receivedValues, [[1],[1,2],[1,2,3]])
        upstream.send(4)
        XCTAssertEqual(receivedValues, [[1],[1,2],[1,2,3],[2,3,4]])
    }

}
