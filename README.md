# Combine Operators

This project provides the sourcecode of some reusable Combine operators
from the Speaking Pace app:

- Publisher.active.swift
- Publisher.slidingWindow.swift
- Publisher.countDuring.swift
- Publisher.movingAverage.swift
- Publisher.slidingAverage.swift


## Sourcecode

You can find the source code in the Operators/Operators/operators/ sub directory.

## Unit Tests

Unit tests are available in the Operators/OperatorsTests/ sub directory.

## Poster

A graphical representation of our operators, their inner structure and how they
are integrated into our Combine-based app architecture is available on our
[Poster in the documentation directory](https://gitlab.com/roland-schmitz/combine-operators/-/raw/master/documentation/Poster.pdf)

## Article and TestFlight

Read our [story on Medium](https://medium.com/@klaus_23359/b524277da47d)
and test the
[Speaking Pace app on TestFlight](https://testflight.apple.com/join/CyFtwk3D)
